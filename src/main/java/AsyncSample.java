import io.reactivex.Flowable;
import io.reactivex.Observable;

import java.util.logging.Logger;

public class AsyncSample {
    public static void main(String[] args) {
//        Logger logger = Logger.getLogger("");
//        Observable.fromCallable(() -> {
//            logger.info(Thread.currentThread().getName() + ": from callable");
//            return "hello world";
//        }).subscribe(item -> {
//            logger.info(Thread.currentThread().getName() + ": subscribe");
//            logger.info("消费 " + item);
//        });
        String [] array = new String[] {"zheng", "yan"};
        Flowable.fromArray(array).count().subscribe(System.out::println);

        int [] ints = new int[] {3,1,4,1,5,9,2,6};
        Flowable.fromArray(3,1,4,1,5,9,2,6).count().subscribe(System.out::println);
    }
}
