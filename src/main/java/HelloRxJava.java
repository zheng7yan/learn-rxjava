import io.reactivex.Flowable;
import io.reactivex.Observable;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.logging.Logger;
import java.util.Arrays;

public class HelloRxJava {
    public static void main(String[] args) {
        Logger logger = Logger.getLogger("");

//        hello("i", "have", "a", "dream");

//        String str = "hello world";
//        Observable<String> observable = Observable.just(str);
//        Observable<String> observable = Observable.just("i", "have", "a", "dream");
//        observable.subscribe(item -> {
//            logger.info("handle item: " + item);
//            logger.info(Thread.currentThread().getName());
//        });
//        Observable.fromArray(3, 1, 4, 1, 5, 9, 2);
        Callable<String> callable = new Callable<String>() {
            @Override
            public String call() throws Exception {
                logger.info("inside callable");
                return "I have a dream.";
            }
        };
        Observable<String> observable = Observable.fromCallable(callable);
        observable.subscribe(item -> {
            logger.info("handle item: " + item);
            logger.info(Thread.currentThread().getName());
        });
    }

    public static void hello(String... args) {
        Flowable.fromArray(args).subscribe(s -> System.out.println("hello " + s + "!"));
    }
}
