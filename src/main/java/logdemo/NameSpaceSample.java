package logdemo;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class NameSpaceSample {

    public static void main(String[] arg) throws SecurityException, IOException {
        // それぞれのロガーを取得します。ルートロガーを取得するにはロガー名に空文字を指定します。
        Logger root = Logger.getLogger("");
        Logger com = Logger.getLogger("com");
        Logger sample = Logger.getLogger("com.sample");
        Logger app = Logger.getLogger("com.sample.App");

        // com.sample.Appの親はcom.sample、com.sampleの親はcom、comの親はルートロガーです。
        System.out.println(app.getParent() == sample);
        System.out.println(app.getParent().getParent() == com);
        System.out.println(app.getParent().getParent().getParent() == root);

        // ルートロガーの親はnullとなります。
        System.out.println(root.getParent());

        // ロガーそれぞれにハンドラーを設定します。
        Formatter formatter = new SimpleFormatter();
        Handler rootHandler = new FileHandler("/tmp/root.log");
        root.addHandler(rootHandler);
        rootHandler.setFormatter(formatter);

        Handler comHandler = new FileHandler("/tmp/com.log");
        com.addHandler(comHandler);
        comHandler.setFormatter(formatter);

        Handler sampleHandler = new FileHandler("/tmp/sample.log");
        sample.addHandler(sampleHandler);
        sampleHandler.setFormatter(formatter);

        Handler appHandler = new FileHandler("/tmp/app.log");
        app.addHandler(appHandler);
        appHandler.setFormatter(formatter);

        // appでメッセージを出力すると親ロガーそれぞれからもメッセージが出力されます。
        app.info("INFOメッセージ");
    }
}
