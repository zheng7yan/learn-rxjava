package logdemo;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class NameSpaceSample3 {

    public static void main(String[] arg) throws SecurityException, IOException {
        // ロガーを3つ取得。ハンドラーの設定、フォーマッターの設定を行う。
        Logger root = Logger.getLogger("");
        Logger sample = Logger.getLogger("sample");
        Logger loggerTreeSample = Logger.getLogger("sample.NameSpaceSample2");
        Handler rootHandler = new FileHandler("/tmp/root.log");
        Handler sampleHandler = new FileHandler("/tmp/sample.log");
        Handler NameSpaceSample3Handler = new FileHandler("/tmp/NameSpaceSample3.log");
        Formatter formatter = new SimpleFormatter();
        rootHandler.setFormatter(formatter);
        sampleHandler.setFormatter(formatter);
        NameSpaceSample3Handler.setFormatter(formatter);
        root.addHandler(rootHandler);
        sample.addHandler(sampleHandler);
        loggerTreeSample.addHandler(NameSpaceSample3Handler);

        // ロガーは全てのログレベルを許可するように設定。（Level.ALLを設定すると全てのログレベルのメッセージの出力を許可します。）
        root.setLevel(Level.ALL);
        sample.setLevel(Level.ALL);
        loggerTreeSample.setLevel(Level.ALL);

        // ハンドラーへログレベルの設定を行う。
        rootHandler.setLevel(Level.WARNING);
        sampleHandler.setLevel(Level.INFO);
        NameSpaceSample3Handler.setLevel(Level.FINE);

        // ログを出力
        root.fine("rootより発信. FINEメッセージ");
        root.info("rootより発信. INFOメッセージ");
        root.warning("rootより発信. WARNINGメッセージ");
        sample.fine("sampleより発信. FINEメッセージ");
        sample.info("sampleより発信. INFOメッセージ");
        sample.warning("sampleより発信. WARNINGメッセージ");
        loggerTreeSample.fine("NameSpaceSample3Handlerより発信. FINEメッセージ");
        loggerTreeSample.info("NameSpaceSample3Handlerより発信. INFOメッセージ");
        loggerTreeSample.warning("NameSpaceSample3Handlerより発信. WARNINGメッセージ");
    }
}