package logdemo;

import java.io.IOException;
import java.util.logging.LogManager;
import java.util.logging.Logger;

public class SettingFileSample {

    public static void main(String[] arg) throws SecurityException, IOException {
        LogManager.getLogManager().readConfiguration(SettingFileSample.class.getResourceAsStream("/logging.properties"));

        Logger logger = Logger.getLogger("sample.SettingFileSample");
        logger.finer("FINERメッセージ");
        logger.fine("FINEメッセージ");
        logger.info("INFOメッセージ");
    }
}
