package logdemo;


import java.io.IOException;
import java.util.function.Supplier;
import java.util.logging.FileHandler;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class BasicLoggingSample {

    public static void main(String[] arg) throws SecurityException, IOException {
        Logger logger = Logger.getLogger(BasicLoggingSample.class.getName());
        logger.setLevel(Level.ALL);

        Handler handler = new FileHandler("/tmp/sample.log");
        handler.setLevel(Level.ALL);
        logger.addHandler(handler);

        Formatter formatter =  new SimpleFormatter();
        handler.setFormatter(formatter);

        logger.log(Level.INFO, "INFOメッセージ");

        logger.finest("FINESTメッセージ");
        logger.finer("FINERメッセージ");
        logger.fine("FINEメッセージ");
        logger.config("CONFIGメッセージ");
        logger.info("INFOメッセージ");
        logger.warning("WARNINGメッセージ");
        logger.severe("SEVEREメッセージ");

        Supplier<String> supplier = new Supplier<String>() {
            @Override
            public String get() {
                return "Supplyメッセージ";
            }
        };
        logger.info(supplier);

        logger.log(Level.WARNING, "エラーが発生しました。", new RuntimeException("ランタイムエラー"));
    }
}
