package logdemo;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class NameSpaceSample2 {

    public static void main(String[] arg) throws SecurityException, IOException {
        // ロガーを3つ取得。ハンドラーの設定、フォーマッターの設定を行う。
        Logger root = Logger.getLogger("");
        Logger sample = Logger.getLogger("sample");
        Logger loggerTreeSample = Logger.getLogger("sample.NameSpaceSample2");
        Handler rootHandler = new FileHandler("/tmp/root.log");
        Handler sampleHandler = new FileHandler("/tmp/sample.log");
        Handler NameSpaceSample2Handler = new FileHandler("/tmp/NameSpaceSample2.log");
        Formatter formatter = new SimpleFormatter();
        rootHandler.setFormatter(formatter);
        sampleHandler.setFormatter(formatter);
        NameSpaceSample2Handler.setFormatter(formatter);
        root.addHandler(rootHandler);
        sample.addHandler(sampleHandler);
        loggerTreeSample.addHandler(NameSpaceSample2Handler);

        // ロガーへログレベルの設定を行う。
        root.setLevel(Level.WARNING);
        sample.setLevel(Level.INFO);
        loggerTreeSample.setLevel(Level.FINE);

        // ハンドラーは全てのログレベルを許可するように設定。（Level.ALLを設定すると全てのログレベルのメッセージの出力を許可します。）
        rootHandler.setLevel(Level.ALL);
        sampleHandler.setLevel(Level.ALL);
        NameSpaceSample2Handler.setLevel(Level.ALL);

        // ログを出力
        root.fine("rootより発信. FINEメッセージ");
        root.info("rootより発信. INFOメッセージ");
        root.warning("rootより発信. WARNINGメッセージ");
        sample.fine("sampleより発信. FINEメッセージ");
        sample.info("sampleより発信. INFOメッセージ");
        sample.warning("sampleより発信. WARNINGメッセージ");
        loggerTreeSample.fine("NameSpaceSample2Handlerより発信. FINEメッセージ");
        loggerTreeSample.info("NameSpaceSample2Handlerより発信. INFOメッセージ");
        loggerTreeSample.warning("NameSpaceSample2Handlerより発信. WARNINGメッセージ");
    }
}
