package logdemo;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Filter;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class FilterSample {

    public static void main(String[] arg) throws SecurityException, IOException {
        // ロガーを作成し、ハンドラーとフォーマッターを設定
        Logger logger = Logger.getLogger("sample1");
        Handler handler = new FileHandler("/tmp/sample.log");
        logger.addHandler(handler);
        handler.setFormatter(new SimpleFormatter());

        // ロガーのログレベルをINFOに設定
        logger.setLevel(Level.INFO);
        logger.info("サンプルメッセージ");                          // 出力される

        // ロガーにフィルターを設定
        Filter filter1 = new Filter() {
            @Override
            public boolean isLoggable(LogRecord record) {
                return record.getMessage().contains("処理件数=");
            }
        };
        logger.setFilter(filter1);
        logger.info("サンプルメッセージ");                          // 出力されない
        logger.info("サンプルメッセージ, 処理件数=1");               // 出力される

        // ハンドラーにフィルターを設定
        Filter filter2 = (record) -> record.getMessage().contains("更新件数=");
        handler.setFilter(filter2);
        logger.info("サンプルメッセージ");                          // 出力されない
        logger.info("サンプルメッセージ, 処理件数=1");               // 出力されない
        logger.info("サンプルメッセージ, 処理件数=1, 更新件数=1");    // 出力される


        handler.setLevel(Level.WARNING);
        logger.info("サンプルメッセージ");                          // 出力されない
        logger.info("サンプルメッセージ, 処理件数=1, warning不起作用?");               // 出力されない
        logger.info("サンプルメッセージ, 処理件数=1, 更新件数=1, warning不起作用?");    // 出力されない
        logger.warning("警告メッセージ, 処理件数=1, 更新件数=1, warning不起作用?");   // 出力される
    }
}