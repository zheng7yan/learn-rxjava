import io.reactivex.Observable;

import java.util.logging.Logger;

public class HelloSample1 {
    public static void main(String[] args) {
        Logger logger = Logger.getLogger("");
        Observable<String> observable = Observable.create(emitter -> {
            logger.info(Thread.currentThread().getName() + " emitting 1st");
            emitter.onNext("1st");
            logger.info("emitting 2nd");
            emitter.onNext("2nd");
            logger.info("emitting 3rd");
            emitter.onNext("3rd");
            logger.info("emitting 4th");
            emitter.onNext("4th");
            logger.info("emitting 5th");
            emitter.onNext("5th");
            logger.info("completing");
            emitter.onComplete();
        });

        observable.map(s -> {
            logger.info(Thread.currentThread().getName() + " handle mapping " + s);
            return s;
        }).subscribe(s -> {
            logger.info(Thread.currentThread().getName() + " consuming " + s);
        });
    }
}
