import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.Subject;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SubjectDemo {
    public static void main(String[] args) throws InterruptedException {
//        Subject<String> subject = PublishSubject.<String>create().toSerialized();
//        subject.map(String::length).subscribe(System.out::println);
//        subject.onNext("hello");
//        subject.onNext("world");
//        subject.onComplete();

//        Scheduler scheduler = Schedulers.single();
        ExecutorService executor = Executors.newSingleThreadExecutor();
        Scheduler scheduler = Schedulers.from(executor);
        Subject<String> subject = PublishSubject.<String>create().toSerialized();
        subject.observeOn(scheduler).doOnNext(
                s -> {
                    System.out.println(Thread.currentThread().getName());
                }
        ).subscribe(s -> {
            System.out.println(String.format("当前线程:%s, 数据:%s", Thread.currentThread().getName(), s));
        });
        Thread.sleep(1000);
        subject.onNext("来自主线程的数据");
        new Thread(() -> {
            subject.onNext("来自子线程1的数据");
        }).start();
        new Thread(() -> {
            subject.onNext("来自子线程2的数据");
        }).start();
        Thread.sleep(2000);
        subject.onComplete();
    }
}
