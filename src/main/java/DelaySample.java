import io.reactivex.Observable;

import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

public class DelaySample {

    static Logger logger = Logger.getLogger("");
    public static void main(String[] args) {
        Observable.just("Alpha", "Beta", "Gamma", "Delta", "Epsilon")
                .delay(3, TimeUnit.SECONDS)
                .subscribe(s -> logger.info(s));
        logger.info("开始计时 ...");
        sleep(5000);
    }

    public static void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}